import {RequestHandler} from "express";

import {Task, UpdateTaskDto} from '../models/task';


export const getAllTasks: RequestHandler = async (req, res, next) => {
    try {
        const tasks = await Task.find()
        console.log(tasks);
        res.status(200).json(tasks);
    } catch {
        res.status(500).json();
}}

export const getTask: RequestHandler<{_id: string}> = async (req, res, next) => {
    const taskId = req.params._id;

    try {
        const task = await Task.findById(taskId)
        console.log(task);
        if(task) {
            res.status(200).json({
                message: 'Task found',
                requestedTask: task
            });
        } else {
            res.status(404).json({
                error: 'Task not found.'
            });
        }} catch {
            res.status(500).json();
        }
}

export const createTask: RequestHandler = async (req, res, next) => {

    const newTask = new Task(req.body);

    try {
        const result = await newTask.save()
        console.log(result);
        res.status(201).json({
            message: 'New task created',
            createdTask: result
            });
        } catch {
            res.status(500).json();
        }
};

export const updateTask: RequestHandler<{_id: string}> = async (req, res, next) => {
    const taskId = req.params._id;

    const body = req.body as UpdateTaskDto;

    try {
        const result = await Task.findOneAndUpdate({_id: taskId}, body, {
            new: true,
            useFindAndModify: false,
            runValidators: true,
        });
        res.status(201).json({
            message: 'Task updated!',
            result
        });
    } catch {
        res.status(500).json();
    }
}

export const deleteTask: RequestHandler<{_id: string}> = async (req, res, next) => {
    const taskId = req.params._id;

    try {
        const deletedTask = await Task.deleteOne({_id: taskId})
        res.status(201).json({
            message: 'Task deleted!',
            deletedTask
        })
    } catch {
        res.status(500).json();
    }
}