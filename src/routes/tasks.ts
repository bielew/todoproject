import { Router } from 'express';

import { createTask, getAllTasks, getTask, deleteTask, updateTask} from '../controllers/tasks';

import { validate } from "class-validator";

import { UpdateTaskDto } from "../models/task";

import { plainToClass } from "class-transformer";

const router = Router();


router.get('/', getAllTasks);

router.get('/:_id', getTask);

router.post('/', validation, createTask);

router.patch('/:_id', validation, updateTask);

router.delete('/:_id', deleteTask);

function validation(req: any, res: any, next: () => void) {
    const taskDto = plainToClass(UpdateTaskDto, req.body);
    validate(taskDto, {
        skipMissingProperties: true,
        forbidUnknownValues: true
    }).then(errors => {
        if (errors.length > 0) {
            console.log('validation failed. Errors:', errors);
            res.status(400).json(errors)
        } else{
            next()
        }
    })
}

export default router;