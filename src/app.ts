import express, {NextFunction, Request, Response} from 'express';
import taskRoutes from './routes/tasks';
import { json } from 'body-parser';
import mongoose from 'mongoose';
require('dotenv').config();

const connectDB = async()=> {
        await mongoose.connect('' + process.env.DB_URL , {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}
connectDB()

const app = express();

app.use(json());

app.use('/tasks', taskRoutes);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).json({message: err.message});
});

app.listen(process.env.PORT || 3000, () => console.log('Server started'));

