import { Schema } from "mongoose";
import mongoose from "mongoose";
import {IsEnum, IsString } from "class-validator";

enum Status {
    ToDo = 'ToDo',
    InProgress = 'InProgress',
    Done = 'Done'
}

interface TaskDto {
    name: string,
    description: string,
    status: Status
}

class UpdateTaskDto {
    @IsString({
        message: '$value is not a string.'
    })
    name!: string;

    @IsString({
        message: '$value is not a string.'
    })
    description!: string;

    @IsEnum(Status, {
        message: '$value is not a valid status. Proper status can be: ToDO, InProgress or Done.'
    })
    status!: Status;
}

interface TaskDoc extends mongoose.Document {
    name: string,
    description: string,
    status: Status
}

interface taskModelInterface extends mongoose.Model<TaskDoc> {
    build(attr: TaskDto): TaskDoc
}

const taskSchema: Schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    status: {
        type: Status,
        required: true,
        default: Status.ToDo
    }
    },
{ timestamps: true}
);

const Task = mongoose.model<TaskDoc, taskModelInterface>('Task', taskSchema);

export {Task, TaskDto, UpdateTaskDto, Status};
